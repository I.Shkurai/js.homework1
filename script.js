var x = 6,
  y = 14,
  z = 4;
result1 = x += y - x++ * z;
document.write(result1 + "<br />");
/* 
К x = 6 добавляется 14 - (6 * 4)
x = (6+14) - (24)
x = -4 
*/

var x = 6,
  y = 14,
  z = 4;
result2 = z = --x - y * 5;
document.write(result2 + "<br />");
/*
z присваивается = 5 - (14 * 5)
z = 5 - 70
z = -65
*/

var x = 6,
  y = 14,
  z = 4;
result3 = y /= x + (5 % z);
document.write(result3 + "<br />");
/*
y = 14 делится на 6 + (5 делится с остачей на 4 = 1)
y = 14/(6+1)
y = 2
*/

var x = 6,
  y = 14,
  z = 4;
result4 = z - x++ + y * 5;
document.write(result4 + "<br />");
/*
result4 = 4 - 6 + (14*5)
result4 = 68 
*/

var x = 6,
  y = 14,
  z = 4;
result5 = x = y - x++ * z;
document.write(result5 + "<br />");
/*
x становится 14 - (6*4)
x = 14 - 24
x = -10
*/
